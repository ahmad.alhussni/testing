<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
 
    public function home()
    {
       $product=Product::all();
       $product_name=Product::getProductName();
       return view('home',['products'=>$product,'product_name'=>$product_name]);
    }

    public function filter(Request $requst)
    {
        $product=$this->process_data($requst->all());
        $product_name=Product::getProductName();
        return view('home',['products'=>$product,'product_name'=>$product_name]);
    }

    function process_data($requst)
    {
         //  three option 
        if((isset($requst['name']))  &&  (isset($requst['from_price'])) && (isset($requst['to_price']))  &&  (isset($requst['level']))){
            
            return $this->search_ByAll($requst['name'],$requst['from_price'],$requst['to_price'],$requst['level']);

        }elseif((isset($requst['name']))  &&  (isset($requst['from_price'])) &&  (isset($requst['level']))){
            
            return $this->search_ByFromPriceAll($requst['name'],$requst['from_price'],$requst['level']);

        }elseif((isset($requst['name']))  &&  (isset($requst['to_price'])) &&  (isset($requst['level']))){

            return $this->search_ByToPriceAll($requst['name'],$requst['to_price'],$requst['level']);

        }// two option
        elseif((isset($requst['name']))  &&  (isset($requst['from_price'])) && (isset($requst['to_price']))){

            return $this->search_ByNamePrice($requst['name'],$requst['from_price'],$requst['to_price']);
    
        }elseif((isset($requst['name']))  &&  (isset($requst['from_price']))){

            return $this->search_ByNameFromPrice($requst['name'],$requst['from_price']);

        }elseif((isset($requst['name']))  &&  (isset($requst['to_price']))){

            return $this->search_ByNameToPrice($requst['name'],$requst['to_price']);

        }elseif((isset($requst['name']))  &&  (isset($requst['level']))){

            return $this->search_ByNameLevel($requst['name'],$requst['level']);

        }elseif((isset($requst['from_price'])) && (isset($requst['to_price'])) &&  (isset($requst['level']))){

            return $this->search_ByPriceLevel($requst['from_price'],$requst['to_price'],$requst['level']);

        }elseif((isset($requst['from_price'])) &&  (isset($requst['level']))){

            return $this->search_ByFromPriceLevel($requst['from_price'],$requst['level']);

        }elseif((isset($requst['to_price'])) &&  (isset($requst['level']))){

            return $this->search_ByToPriceLevel($requst['to_price'],$requst['level']);
        }
        elseif(isset($requst['name'])){

            return $this->search_ByName($requst['name']);

        }elseif((isset($requst['from_price'])) && (isset($requst['to_price']))){

            return $this->search_ByPrice($requst['from_price'],$requst['to_price']);

        }elseif((isset($requst['from_price']))){

            return $this->search_ByFromPrice($requst['from_price']);

        }elseif((isset($requst['to_price']))){

            return $this->search_ByToPrice($requst['to_price']);
        }
        elseif(isset($requst['level'])){

            return $this->search_ByLevel($requst['level']);
        }
    }

    /*
    * function for get data
    */
    // for three option 
    function search_ByAll($name,$from_price,$to_price,$level)
    {
        return Product::where('name','like',$name)->whereBetween('price', [$from_price, $to_price])->where('level',$level)->get();
    }

    function search_ByFromPriceAll($name,$from_price,$level)
    {
        return Product::where('name','like',$name)->where('price', '>=', $from_price)->where('level',$level)->get();
    }

    function search_ByToPriceAll($name,$to_price,$level)
    {
        return Product::where('name','like',$name)->where('price', '<=', $to_price)->where('level',$level)->get();
    }
    // for two option
    function search_ByNamePrice($name,$from_price,$to_price)
    {
        return Product::where('name','like',$name)->whereBetween('price', [$from_price, $to_price])->get();
    }

    function search_ByNameFromPrice($name,$from_price)
    {
        return Product::where('name','like',$name)->where('price', '>=', $from_price)->get();
    }

    function search_ByNameToPrice($name,$to_price)
    {
        return Product::where('name','like',$name)->where('price', '<=', $to_price)->get();
    }

    function search_ByNameLevel($name,$level)
    {
        return Product::where('name','like',$name)->where('level',$level)->get();
    }

    function search_ByPriceLevel($from_price,$to_price,$level)
    {
        return Product::whereBetween('price', [$from_price, $to_price])->where('level',$level)->get();
    }

    function search_ByFromPriceLevel($from_price,$level)
    {
        return Product::where('price', '>=', $from_price)->where('level',$level)->get();
    }

    function search_ByToPriceLevel($to_price,$level)
    {
        return Product::where('price', '<=', $to_price)->where('level',$level)->get();
    }
    // for one option
    function search_ByName($name)
    {
        return Product::where('name','like',$name)->get();
    }

    function search_ByPrice($from_price,$to_price)
    {
        return Product::whereBetween('price', [$from_price, $to_price])->get();
    }

    function search_ByFromPrice($from_price)
    {
        return Product::where('price', '>=', $from_price)->get();
    }

    function search_ByToPrice($to_price)
    {
        return Product::where('price', '<=', $to_price)->get();
    }

    function search_ByLevel($level)
    {
        return Product::where('level',$level)->get();
    }
 
}
