<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    static public function getProductName()
    {
       return \DB::table('products')->select('name')->distinct('name')->get();  
    }
}
