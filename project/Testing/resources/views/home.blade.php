<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Products</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
      $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
    </script>
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
<a class="navbar-brand" href="{{route('home')}}"><button class="btn-lg"> show products</button></a>
    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto"></ul>
       <form action="{{route('filter')}}" method="get" class="form-inline my-2 my-lg-12">
            <div class="input-group mr-sm-2">
                <div class="form-control mr-sm-2 ">
                    <select id="level" name="name" style="border: 0  transparent;">
                        <option value="" selected disabled>choose Name</option>
                        @if(isset($product_name) && !empty($product_name)) 
                            @foreach ($product_name as $p)
                                @if(isset($p->name) && !empty($p->name)) 
                                <option value="{{$p->name}}">{{$p->name}}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="input-group mr-sm-2">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <div class="form-control mr-sm-2 ">
                    <select id="level" name="from_price" style="border: 0  transparent;">
                        <option value="" selected disabled>Select Price From</option>
                        <option value="0">0</option>
                        <option value="10">10</option>
                        <option value="100">100</option>
                        <option value="1000">1000</option>
                    </select>
                </div>
            </div>
            
            <div class="input-group mr-sm-2">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <div class="form-control mr-sm-2 ">
                    <select id="level" name="to_price" style="border: 0  transparent;">
                        <option value="" selected disabled>Select Price To</option>
                        <option value="1000">1000</option>
                        <option value="10000">10000</option>
                        <option value="100000">100000</option>
                        <option value="1000000">1000000</option>
                    </select>
                </div>
            </div>

            <div class="form-control mr-sm-2 ">
                <select id="level" name="level" style="border: 0  transparent;">
                    <option value="" selected disabled>Choose Level</option>
                    <option value="L1">L1</option>
                    <option value="L2">L2</option>
                    <option value="L3">L3</option>
                </select>
            </div>

            <button class="btn btn-secondary my-2 my-sm-0" type="submit">Apply Filters</button>
        </form>
    </div>
</nav>
<body style="background-color:#eef0f1;">

<div class="card m-4">
    <div class="col">

        <div class="col-2 m-3" id="searchField">
            <input name="searchField" id="search" class="form-control" type="text" placeholder="Search">
        </div>

        <div class="table-responsive m-2">
            <table class="table table-striped table-bordered display table-hover">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Level</th>
                </tr>
                </thead>
                <tbody id="myTable">
               @if(isset($products) && !empty($products)) 
                    @foreach ($products as $product)
                        <tr>
                           <td>{{ $product->id }}</td>
                           <td>{{ $product->name }} </td>
                           <td>{{ $product->price }} </td>   
                           <td>{{ $product->level }} </td> 
                        </tr>
                    @endforeach
                @endif
              <?php  ?>
                </tbody>
                <tfoot>
                <th>Id</th>
                <th>Name</th>
                <th>Price</th>
                <th>Level</th>
                </tfoot>
            </table>
        </div>
    </div>
</div>

</body>
</html>




 
